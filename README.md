# Rock Paper Scissor
Rock Paper Scissors is a familiar game which is played by two people. Where based on certain instances we will win, lose or either draw. The one with more points finally wins the game.

## Description
This game is basically built using JavaScript, HTML, CSS.

Here is the link for playing:https://we-rock-paper-scissorss-game.netlify.app/

Rock paper scissors here is played just by clicking on the option that we choose and the result comes directly on to your screen. The result is obtained based on your choice and the random choice by computer.


## Visuals
![Game Screenshot](images/screenshot.png)

## Installation
### Clone the repository: 
- Clone or download the repository 'Rock-Paper-Scissors' or and open 'index.html' to start playing

or

Follow the link: https://we-rock-paper-scissorss-game.netlify.app/ 

## How to play
1. As you click on your choosed option the computer takes random option and evaluates the result.
2. There are three chances to win:
- If you get paper and computer gets rock
- If you get rock and computer gets scissors
- If you get scissors and computer gets paper
3. And the in rest cases you lose while you may also get draw if your choice and computer choice is same.
4. And that is all the game. Go on playing until your target, the one with highest score wins!. 

## Resources
JavaScript Documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide





